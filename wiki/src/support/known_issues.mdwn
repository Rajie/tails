[[!meta title="Known issues"]]

- For issues with graphics cards, see our [[known issues with graphics cards|known_issues/graphics]].
- To learn what you can do with Tails, refer to our [[documentation|doc]].
- For more general questions about Tails, refer to our [[FAQ|support/faq]].

[[!toc levels=3]]

Problems starting Tails
=======================

This section lists known problems with specific hardware. To report a problem
with different hardware see our [[report guidelines when Tails does not
start|doc/first_steps/bug_reporting#does_not_start]].

<a id="problematic-usb-sticks"></a>

Problematic USB sticks
----------------------

<a id="pny"></a>

### PNY

<!--
Last updated: 2014-08-02

I asked help desk about it.

PNY is quite popular among WB reporters:

    436 SanDisk
    187 Kingston
    119 USB
     88 Generic
     82 Lexar
     79 Intenso
     57 Verbatim
     56 TOSHIBA
     50 PNY

If it was still problematic on all PNY USB sticks, this issue would
still be loud on help desk. Is it?
-->

When installing Tails, PNY USB sticks have problems with the constant write load
and are prone to failure.

<a id="aegis"></a>

### Aegis Secure Key

<!--
Last updated: 2015-04-10

wb://bf7230b2d53d28cfb7f063d897221b60 is from someone who was able to boot
from an Aegis Secure Key USB 3.0 and proposes to help us document it.

I asked the reported about it.
-->

* Aegis Secure Key USB 2.0

During the boot process, USB is briefly powered off, that causes Aegis hardware-encrypted USB sticks to lock down, and the PIN must be entered again (fast) in order to complete the boot.

* Aegis Secure Key USB 3.0

This USB stick doesn't start Tails at all, the USB 2.0 workaround is not working for that hardware.

<a id="datatraveler-2000"></a>
<a id="datatraveler-100-g3"></a>

### Kingston DataTraveler 2000, Kingston DataTraveler 100 G3

<!--
Last updated: 2018-09-18
Success report received on 2020-01-04
-->

Starting Tails from a Kingston DataTraveler 2000 or DataTraveler 100G3 may
not work.

<a id="mac"></a>

Mac
---

<!--
Last updated:
- MacBook Air 3,2: 2014-06-27
- MacBook Pro 5,1: 2015-06-19
- MacBook Pro 4,1: 2014-08-15
- MacBook Air Retina 2019: 2019-11-01
- MacBook Pro 8,3: 2019-11-01
-->

* Any Mac with 32-bit EFI might not start on Tails.
  You can check if a given Mac is 32-bit
  or 64-bit EFI on that list:
  <https://www.everymac.com/mac-answers/snow-leopard-mac-os-x-faq/mac-os-x-snow-leopard-64-bit-macs-64-bit-efi-boot-in-64-bit-mode.html>

* MacBook Air Retina 2018 and 2019

  To start Tails successfully, [[add the following boot option when
  starting Tails|doc/advanced_topics/boot_options]]:

      modprobe.blacklist=thunderbolt

  Still, the trackpad and keyboard may not work. You may use an external
  mouse and keyboard.

* MacBook Pro 2016 and 2017: the keyboard, trackpad and Touch Bar may
  not work. If they do, please let us know so we can update this page.

* MacBook Pro 8,3 17" (AMD Radeon HD6770M and Intel HD Graphics 3000)

  If Tails fails to start, [[add the following boot option when
  starting Tails|doc/advanced_topics/boot_options]]:

      radeon.modeset=0

* MacBook Pro 5,1 17" (Nvidia GeForce 9400M)

  To make the display work properly, [[add the following boot option when
  starting Tails|doc/advanced_topics/boot_options]]:

      nouveau.noaccel=1

* MacBook Air 3,2 (A1369 EMC 2392) freezes when booting Tails in
  UEFI mode.

* Mac Pro Tower and MacBook Pro 4,1 (both from early 2008)
  fail to start on Tails.

<a id="pc">

PC
--

<a id="lockup"></a>

### "Soft lockup" error

<!--
Last updated: 2019-05-20
-->

On some laptops, Tails starts with the following error:

<p class="pre">soft lockup - CPU#<span class="command-placeholder">N</span> stuck for <span class="command-placeholder">N</span>s</p>

To start Tails successfully, [[add the following boot option when starting
Tails|doc/advanced_topics/boot_options]]:

    acpi_rev_override=1 nouveau.modeset=0

<a id="sg-black-screen"></a>

### Acer Travelmate 8573T-254G50M

<!--
Last updated: 2013-08-08
-->

Booting from DVD works fine, but does not start from USB sticks.

This problem might be corrected in Tails 1.1 and newer: please report
your test results back to us.

### Acer Aspire 5315-ICL50

<!--
Last updated: 2015-04-10
-->

Does not start on USB sticks.

### AMD Ryzen with Vega graphics cards

See [[support/known_issues/graphics#amd-vega]].

### ASUS VivoBook X202E

<!--
Last updated: 2013-07-20
-->

Legacy support needs to be enabled in order to start Tails. To enable
legacy boot support, enable 'Launch CSM' under boot (menu).

This problem might be corrected in Tails 1.1 and newer: please report
your test results back to us.

### Dell Chromebook LULU

<!--
Last updated: 2018-01-10
-->

To start Tails successfully, [[add the following boot option when starting
Tails|doc/advanced_topics/boot_options]]:

    nomodeset

### Dell Latitude E5250

<!--
Last updated: 2019-04-05
-->

Does not start on Tails USB sticks.

### Dell Latitude E6430 and E6230

<!--
Last updated: 2018-06-14 (wb://7653aff4f415e996567233d8c088da08)
-->

Does not start on USB sticks.

With BIOS versions A03 06/03/2012 (and A09, A11, and A12)

Error message: `Invalid partition table!`

Workaround (at least with BIOS versions A09, A11, and A12): just hit enter
and it will continue with the boot.

### Dell XPS L702X/03RG89, Samsung RV520, Samsung Series 7 Chronos

<!--
Last updated:
- Dell XPS L702X/03RG89: 2012-08-22
- Samsung RV520: 2012-12-21
- Samsung Series 7 Chronos: 2014-02-28
-->

Does not start on USB sticks.

We were reported that the legacy BIOS shipped on these systems doesn't
know how to deal with the GPT partition scheme used in Tails.

This problem might be corrected in Tails 1.1 and newer: please report
your test results back to us.

<https://www.dell.com/community/Laptops-General-Read-Only/GPT-Bootable-Bios-Optimus-Switch-Are-Necessary-For-L502x-L702x/m-p/3699920>

### HP Compaq dc5750 Microtower

<!--
Last updated: 2015-02-10
Specs: https://support.hp.com/us-en/document/c01110206
-->

Does not start Tails 1.2.3.

### HP Compaq 615

<!--
Last updated: 2013-11-05
Specs: https://support.hp.com/gb-en/document/c01768616
-->

You need to update the firmware to its latest version in order to start from a
USB stick.

### HP Compaq CQ60-214DX

<!--
Last updated: 2018-02-16
Specs: https://www.cnet.com/products/hp-compaq-presario-cq60-615dx/specs/
-->

Tails 3.3 does not start.

### HP Pavilion 15-ab277ca

<!--
Last updated: 2018-01-15
Worked in 3.0 (wb://b485a1cfa7f7cc1073a70b31f428097c)
-->

Tails 3.3 restarts during startup and never starts successfully.

### HP ProBook

<!--
Last updated: 2016-05-14
-->

With UEFI enabled, when choosing a boot device, select `Boot From EFI File` and
then `Filesystem Tails` and `EFI/BOOT/BOOTX64.EFI`.

That workaround applied to, at least, the following HP ProBook:

* 5330m
* 4330s
* 6560b

### Lenovo IdeaPad Y410p

<!--
Last updated: 2014-08-03
Specs: https://www.lenovo.com/us/en/laptops/lenovo/y-series/y410p/
-->

Does not start Tails 1.1 from USB installed manually in Linux.

### Lenovo IdeaPad z585

<!--
Last updated: 2014-08-05
Specs: https://www.cnet.com/products/lenovo-ideapad-z585-15-6-a8-4500m-6-gb-ram-750-gb-hdd/
-->

Goes back continuously to Boot Loader on Tails installed on DVD.

### Microsoft Surface Laptop 3

<!--
Last updated: 2020-07-03
-->

The keyboard and mouse do not work on this laptop.
If you find a workaround, please let us know.

### Clevo W258CU, ThinkPad X121e, T420i, T410, T520, W520, T530, T60, E325, and E530

<!--
Last updated:
- Clevo W258CU: 2014-03-29
- ThinkPad X121e: 2014-02-10
  Specs: https://www.cnet.com/products/lenovo-thinkpad-x121e-3045-11-6-core-i3-2367m-windows-7-pro-64-bit-4-gb-ram-320-gb-hdd-series/
- ThinkPad T420i: 2014-06-06
  Specs: https://www.cnet.com/products/lenovo-thinkpad-t420i-4178-14-core-i3-2310m-windows-7-pro-64-bit-4-gb-ram-320-gb-hdd-series/
- ThinkPad T520: 2012-10-11
  Specs: https://www.cnet.com/products/lenovo-thinkpad-t520/
- ThinkPad W520: 2014-02-17
  Specs: https://www.cnet.com/products/lenovo-thinkpad-w520/
- ThinkPad T60: 2018-04-30
  Specs: https://www.cnet.com/products/lenovo-thinkpad-t60/specs/
- ThinkPad E325: 2013-02-28
  Specs: https://www.cnet.com/products/lenovo-thinkpad-edge-e325-1297-13-3-e-350-windows-7-pro-64-bit-4-gb-ram-320-gb-hdd-series/
- ThinkPad E530: 2014-03-17
  Specs: https://www.cnet.com/products/lenovo-thinkpad-edge-e530/
-->

These machines do not start on USB sticks
due to a firmware limitation.

This problem might be corrected in Tails 1.1 and newer: please report
your test results back to us.

### System76 Oryx Pro

<!--
Last updated: 2019-05-02
Specs: https://system76.com/laptops/oryx
-->

This machine does not start from a Tails USB stick: the Linux kernel
included in at least Tails 3.11 to 3.13.1, inclusive, does not support
the hardware USB controller.

<a id="wi-fi"></a>

Wi-Fi issues
============

[[!inline pages="doc/anonymous_internet/networkmanager/no-wifi.inline" raw="yes" sort="age"]]

* Check in the following sections
  if there is a workaround to get your Wi-Fi interface work in Tails.

<a id="wi-fi-workarounds"></a>

Knowing the model of your Wi-Fi interface
-----------------------------------------

1. Open <span class="application">Terminal</span> and execute the following command:

       lspci -v | grep "Network controller"

88W8897 [AVASTAR] 802.11ac Wireless
-----------------------------------

<!--
Last updated: 2018-09-19
-->

On some computers with a Marvell Avastar 88W8897 Wi-Fi adapter, such
as some Microsoft Surface Pro models, Tails fails to connect to
Wi-Fi networks.

If you experience this problem, you can try to [[disable MAC address
spoofing|doc/first_steps/welcome_screen/mac_spoofing]] that sometimes
fixes it.

RTL8723BE PCIe Wireless Network Adapter
---------------------------------------

<!--
Last updated: 2017-12-24
-->

On some computers with a RTL8723be Wi-Fi adapter, Tails might fail to discover
Wi-Fi networks, provide unreliable Wi-Fi connections, or have poor Wi-Fi
performance.

You can to [[add the following boot options when starting
Tails|doc/advanced_topics/boot_options]] until you find a
combination that works for you. The exact combination of options that works
depends on the computer:

- <span class="command">rtl8723be.fwlps=0 rtl8723be.ips=0</span>

- <span class="command">rtl8723be.ant_sel=1</span>

- <span class="command">rtl8723be.ant_sel=2</span>

- <span class="command">rtl8723be.ant_sel=1 rtl8723be.fwlps=0 rtl8723be.ips=0</span>

- <span class="command">rtl8723be.ant_sel=2 rtl8723be.fwlps=0 rtl8723be.ips=0</span>

<a id="broadcom-sta-dkms"></a>

Broadcom Wi-Fi network interface needing `broadcom-sta-dkms`
------------------------------------------------------------

<!--
Last updated: 2019-01-29
-->

Some Broadcom Wi-Fi interfaces require the
[`wl`](https://wiki.debian.org/wl) driver, provided by the
`broadcom-sta-dkms` Debian package, to work in Tails.

The `wl` driver is not included in Tails because it is proprietary.

You device requires the `wl` driver if it is in the list of devices
supported by the `broadcom-sta-dkms` package on the corresponding
[Debian wiki page](https://wiki.debian.org/wl). If you find your device
in the list of supported devices, then it is impossible to use your
Wi-Fi card in Tails.

## Lenovo Legion Y530

<!--
Last updated: 2019-05-10
-->

Wi-Fi adapter does not work.

To fix this issue, [[add the following boot option when
starting Tails|doc/advanced_topics/boot_options]]:

    modprobe.blacklist=ideapad_laptop

## RTL8821CE

<!--
Last updated: 2020-02-21
-->

The Realtek RTL8821CE Wi-Fi adapter is not supported in Linux yet.

This problem is tracked by [[!debbug 917941]].

Security issues
===============

<a id="video-memory"></a>

Tails does not erase video memory
---------------------------------

Tails doesn't erase the [[!wikipedia Video_RAM_(dual-ported_DRAM) desc="video memory"]] yet.
When one uses Tails, then restarts the computer into another operating
system, that other operating system might, for a moment, display the last
screen that was displayed in Tails.

Shutting down the computer completely, instead of restarting it,
might allow the video memory to empty itself.

See [[!tails_ticket 5356 desc="Erase video memory on shutdown"]].

<!--

Note: If we enable again this section in the future, we should link to
it from /doc/about/fingerprint.mdwn.

<a id="fingerprint"></a>

Browser fingerprint
===================

There are known differences between the fingerprints of <span
class="application">Tor Browser</span> inside and outside of Tails:

For more detail, see our [[documentation on the Tails
fingerprint|doc/about/fingerprint]].

-->

Other issues
============

<a id="openpgp-import"></a>

Importing OpenPGP public keys using the *Passwords and Keys* utility does nothing
---------------------------------------------------------------------------------

[[!inline pages="support/known_issues/import_broken_in_seahorse.inline" raw="yes" sort="age"]]

Boot Loader has display issues
------------------------------

<!--
Last updated: 2014-06-27

I asked some ThinkPad X230 users to confirm.
-->

Since Tails 1.1, on some hardware (ThinkPad X230, MacBook Pro 8,1),
the Boot Loader is not displayed properly. Tails starts fine, though.

Touchpad configurations
-----------------------

### Acer TravelMate B113 - ETPS/2 Elantech Touchpad

<!--
Last updated: 2013-08-15
Specs: https://www.cnet.com/products/acer-travelmate-b113-e-2419-11-6-celeron-1017u-4-gb-ram-320-gb-hdd/
-->

    synclient FingerPress=256;
    synclient TapButton3=0;
    synclient Clickpad=1;
    synclient VertTwoFingerScroll=1;
    synclient FingerLow=1;
    synclient FingerHigh=1;

### Acer C720 - Cypress APA Touchpad

<!--
Last updated: 2016-08-20
-->

    synclient FingerLow=5;
    synclient FingerHigh=5;

Bluetooth devices don't work
----------------------------

Bluetooth is not enabled in Tails for security reasons.

<a id="automatic_upgrade_fails"></a>
<a id="partial-upgrade"></a>

Tails fails to start or behaves weirdly after an automatic upgrade
------------------------------------------------------------------

<!--
Last updated: 2019-07-31
-->

Sometimes, after an automatic upgrade, your Tails might either:

- Fail to start. Often after the message:

  <pre>Loading, please wait...</pre>

- Behave weirdly. For example, your keyboard does not work or you cannot
  connect to a network.

To fix this problem, you can [[update your Tails
manually|doc/upgrade/#manual]].

Note that your Persistent Storage will be safely preserved.

<a id="persistence-disappears"></a>

<em>Persistent</em> folder disappears and data of the Persistent Storage is unavailable
-----------------------------------------------------------------------------------------

Sometimes, the *Persistent* folder is missing and
all the data of the Persistent Storage is unavailable.

Most likely this means that the *persistence.conf*
file has disappeared. However, the files in the
*Persistent* folder and all the other data in the Persistent Storage
should still exist.

If you encounter this problem:

1. Unlock the Persistent Storage in
   the Welcome Screen when starting Tails.

1. Choose
   <span class="menuchoice">
     <span class="guimenu">Applications</span>&nbsp;▸
     <span class="guisubmenu">Tails</span>&nbsp;▸
     <span class="guimenuitem">Configure persistent volume</span></span>.

1. If you are asked to enter a passphrase, enter the same passphrase that you
   use to unlock your Persistent Storage.

1. Turn on the features of the Persistent Storage that you had turned on previously.

1. Click <span class="button">Save</span>, then restart Tails.

Your *Persistent* folder and all the data of the Persistent Storage
should be restored.

If the above instructions do not fix the problem, you might need to fix the
ownership of TailsData_unlocked.

To fix the ownership of TailsData_unlocked, set an administration password and
execute the following command in a terminal:

`sudo chown root:root /live/persistence/TailsData_unlocked`

<a id="restart-shell"></a>

## Icons and information located on the top right corner of the screen disappeared

Sometimes, some of the icons located on the top right corner of the
screen are not displayed entirely, or at all. For example,
the icon that allows to change to another keyboard layout
may be hidden. Other information, such as the clock, may not
be visible.

Press <span class="keycap">Alt+F2</span> and type `r` to restart GNOME
Shell, which often solves this problem.

See [[!tails_ticket 10576]] for more details.

Some languages do not have the correct keyboard layout set by default
---------------------------------------------------------------------

<!--
Last updated: 2017-08-07
-->

When selecting some languages in the Welcome Screen, the default associated
keyboard layout fallbacks to US.

To use the right keyboard layout during a session, set it in the Welcome
Screen after having set the language. When the desktop has started, apply
the keyboard layout by clicking on the `en` systray icon.

Affected language codes are `AYC`, `BHB`, `BRX`, `CMN`, `HAK`, `HNE`, `LIJ`,
`LZH`, `MHR`, `NAN`, `NHN`, `QUZ`, `SGS`, `SHS`, `TCY`, `THE`, `UNM`, `WAE` and
`YUE`.

## The OpenPGP passphrase prompt steals the keyboard and mouse focus

<!--
Last updated: 2018-01-15
-->

This happens when the GnuPG feature of the Persistent Storage was turned on before
Tails 2.3. To fix this problem, execute the following command in
a
[[terminal|doc/first_steps/introduction_to_gnome_and_the_tails_desktop#terminal]]:

	echo "no-grab" >> ~/.gnupg/gpg-agent.conf

<a id="utc"></a>

## Problems when the system clock goes backwards

When connecting to Tor, Tails sets the system time to the current time
in the [[!wikipedia Coordinated_Universal_Time]] (UTC) timezone.
Many operating systems, such as Linux and macOS, write time expressed
in UTC to the hardware clock of the computer. But, Windows instead
writes time expressed in the local timezone to the hardware clock of
the computer. So, if you are east of the United Kingdom (which is in the UTC timezone) on
a computer that also runs Windows, Tails will make the system clock go
backwards during startup. Unfortunately, this might trigger software
errors in Tails.

For example, when the system clock goes backwards:

<ul>
<li>The <span class="guimenu">Applications</span> menu might stop
working.</li>
<li>[[Connecting to Tor using `obfs4` bridges|doc/first_steps/welcome_screen/bridge_mode]]
might be impossible and <span class="application">Tor Launcher</span>
might get stuck on <span class="guilabel">Loading Network
Consensus</span>.</li>
</ul>

To solve this problem permanently on a Windows computer,
[set the hardware clock of the computer to UTC](https://wiki.archlinux.org/index.php/System_time#UTC_in_Windows).

## Lenovo ThinkPad 11e

<!--
Last updated: 2018-03-22
Specs: https://www.cnet.com/products/lenovo-thinkpad-11e/specs/
-->

Tails may be unstable and stop working regularly on some Lenovo
ThinkPad 11e laptops, such as the 2015 model.

To fix this problem, [[add the following boot option when
starting Tails|doc/advanced_topics/boot_options]]:

    intel_idle.max_cstate=1

<a id="usb-gigabyte"></a>

## USB devices are not working on some [GIGABYTE](https://www.gigabyte.com/) motherboards

<!--
Last updated: 2019-06-02
-->

To workaround this issue, you can try to:

- Use other USB ports. For example, use USB 2 ports instead of USB 3
  ports and vice versa.

- Enable these 3 options in the BIOS

  - xHCI handoff
  - EHCI handoff
  - IOMMU controller

- [[Add the following boot option when
  starting Tails|doc/advanced_topics/boot_options]]:

      amd_iommu=on iommu=pt

- Disable IOMMU in the BIOS then enable it again.

Some of these workarounds may disable some of the USB ports.

## Graphics corruption in in Tor Browser and Thunderbird

<!--
Last updated: 2020-02-22
-->

On some computers, _Tor Browser_ and _Thunderbird_ are displayed incorrectly:

 - The application window is initially empty and white.
 - The icons and menus only appear when hovered by the mouse.
 - The contents of the window is only displayed in stripes when scrolling.

To workaround this issue, [[add the following boot option when
starting Tails|doc/advanced_topics/boot_options]]:

    intel_iommu=off

This problem affects at least the following computers:

 - MacBookPro11,5
 - other MacBooks with a Radeon R9 graphics adapter

### Acer Swift 3

In order to allow Tails to use all available memory,
start Tails from the *F2 EFI setup* firmware entry.
